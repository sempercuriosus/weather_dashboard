# Weather Dashboard
<!-- Title  -->

---

## Table of Contents

<!-- Table of Contents -->

- [About The Project](#about_project)
- [Getting Started](#getting_started)
- [Deployment Location](#deployment_location)
- [Challenges](#challenges)
- [Author Credit](#author_credit)
- [Acknowledgments](#acknowledgments)
- [Resources](#resources)
- [Final Note](#final_note)

---

## About The Project <a id="about_project"></a>

<!-- About the Project -->
The design for this app is to provide a multiple city forcast for cities of the user's choice. 

### Acceptance Criteria
GIVEN a weather dashboard with form inputs

[x] **WHEN** I search for a city
**THEN** I am presented with current and future conditions for that city and that city is added to the search history

[x] **WHEN** I view current weather conditions for that city **THEN** I am presented with the city name, the date, an icon representation of weather conditions, the temperature, the humidity, and the the wind speed

[x] **WHEN** I view future weather conditions for that city **THEN** I am presented with a 5-day forecast that displays the date, an icon representation of weather conditions, the temperature, the wind speed, and the humidity

[x] **WHEN** I click on a city in the search history **THEN** I am again presented with current and future conditions for that city

### Built With

<!-- Built With -->
- HTML
- CSS
- Javascript

### Features

<!-- Features -->
* Search for a location and get back the current conditions and a five-day forecast!
* Each time a successful search is performed the `Searched City` will appear in the list below the `Search Button`. 
* The `Searched City` history is also searchable; click on the `Searched City` is a button too.

---

## Getting Started <a id="getting_started"></a>

<!-- Getting Started  -->

### Prerequisites & Dependencies

<!-- Prerequisites & Dependencies-->

- (Open Weather Map API)(https://home.openweathermap.org/)
- https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;400;500;600;700&display=swap
- https://cdn.jsdelivr.net/npm/dayjs@1.11.3/dayjs.min.js"

### Installation

<!-- Installation -->
This is a web application, so there is no installation needed! 

### Usage

<!-- Usage -->
* Click into the `Search` field and enter in a `City` or a `City, State`
* Click the `Search` Button to execute the search. 
* The `Searched City` will become a clickable button to recall the previous searches.

---

## Deployment Location <a id="deployment_location"></a>

<!-- Deployment Location -->
<img width="1709" alt="application_preview" src="https://github.com/sempercuriosus/weather_dashboard/assets/31829327/0c98c5dd-7c40-42c5-b0d2-f9ecf4d6f149">

[Application Preview](https://sempercuriosus.github.io/weather_dashboard/)

<img width="1709" alt="Screenshot 2023-08-27 at 07 05 36" src="https://github.com/sempercuriosus/weather_dashboard/assets/31829327/ec2f4a9a-b8ca-4f71-8866-9b3cc686e9c8">

[Application Preview Image](./assets/readme_images/application_preview.png)

---

## Challenges <a id="challenges"></a>

<!-- Challenges -->
1. Figuring out how to avoid overflow when the 100% or 100vh would automatically overflow the y axis.
2. My biggest challeng here was trying to wrap my head around what Fetch was doing and how it was returning data. I think I understand it now. When you are making a fetch request, you cannot return data in a sychronous manner, so initially when I was trying to do this I was trying to return the data to my function that called it. Then I could work with the object from there, however, that does not work. You need to use the data in the scope of the `.then` promise being made. Therefore, you need to shuffle it out to where you need it as soon as it is available. If not, it will not be there. 
3. Something I solved on my own, before I really even got into writing this app, was storing the data from the first call into an array of objects, such that, the first call only needs to be made one time per city.

---


## Author Credit <a id="author_credit"></a>

<!-- Author Credits -->

Eric (Semper Curiosus)

---

## Acknowledgments <a id="acknowledgments"></a>

<!-- Acknowledgments -->
I used the Xpert some, but do not believe I include any code snippets from it. If I did, they were not more than a line or two.

---

## Resources <a id="resources"></a>

<!-- Resources -->
### General Dev. Resources 
- https://caniuse.com/ciu/about
- https://thomaspark.co/2015/11/bob-ross-color-palette-in-css/
- https://dev.to/frehner/css-vh-dvh-lvh-svh-and-vw-units-27k4#the-new-css-units
- https://developer.mozilla.org/en-US/docs/Web/CSS/flex
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Cheatsheet
- https://stackoverflow.com/questions/47604040/how-to-get-data-returned-from-fetch-promise
- https://www.freecodecamp.org/news/javascript-object-destructuring-spread-operator-rest-parameter/
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/throw
- https://stackoverflow.com/questions/41045548/merging-changes-from-master-into-my-branch
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find


### API Calls
- https://openweathermap.org/api/one-call-3
- https://openweathermap.org/api/one-call-3#data
- https://openweathermap.org/api/geocoding-api


---

## Final Note <a id="final_note"></a>

<!-- Final Note -->
This project was the first one that I really got to see the true scope of Web Development. One can spend a LOT of time learning how to program on a local computer, but then, add to that the nature of the web, UI/UX design, and I am now just scratcing the surface here of the Web Dev world.

---
