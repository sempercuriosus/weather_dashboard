/** 
 * Weather Dashboard
 * @author Eric Hulse - Semper Curiosus
 */

console.log("Weather Dashboard Script Loaded!");

// API KEY
const apiKey = "bce8416301b0fc2dcd2d7e277dd9c88f";

// #region Document Query Selectors
//
let searchButton = document.querySelector("#search-button");
let searchInput = document.querySelector("#search-input");
let searchHist = document.querySelector("#search-hist");
let elCurrentCondCard = document.querySelector("#current-conditions-card");
let elCurrentCondCity = document.querySelector("#current-conditions-city");
let elCurrentCondState = document.querySelector("#current-conditions-state");
let elCurrentDate = document.querySelector("#current-date");
let elCurrentCondTemp = document.querySelector("#current-conditions-temp");
let elCurrentCondFeelsLike = document.querySelector("#current-conditions-temp-feels-like");
let elCurrentCondWindSpeed = document.querySelector("#current-conditions-wind-speed");
let elCurrentCondHumidity = document.querySelector("#current-conditions-humidity");
let elCurrentCondIcon = document.querySelector("#current-conditions-icon");
let elFiveDayCard = document.querySelector("#five-day-card");
//
// #endregion


// #region Lat and Long Search
//


/**
 * Search History includes a historical list of searched for cities, along with metadata, such that, the call to get that data is not needed
 * @property {array} cities - an array of {cityMetadata} objects
 * 
 */
let searchHistory = {
    "cities": [],
};


/**
 * Contains a list of a searched for city's metadata returned from the api call
 * @property {string} cityName - the searched for city's cityName
 * @property {string} state - the searched for city's state
 * @property {string} country - the searched for city's country
 * @property {string} lattitude - the searched for city's lattitude
 * @property {string} longitude - the searched for city's longitude
 * 
 * 
 */
const cityMetadata = {
    "city": "",
    "state": "",
    "country": "",
    "lattitude": 0,
    "longitude": 0,
};


/**
 * Contains the parameters used in getting the geocoding api call
 * @property {string} urlBase - The base url of the api call the search will be built from
 * @property {string} cityNameParam - The search parameter the api call needs
 * @property {string} cityNameValue - Value for the cityNameParam search parameter 
 * @property {string} stateParam - The search parameter the api call needs
 * @property {string} stateValue - Value for the stateParam search parameter 
 * @property {string} appidParam - The search parameter the api call needs
 * @property {string} appidValue - Value for the appidParam search parameter 
 * 
 */
const searchLatAndLong = {
    "urlBase": "https://api.openweathermap.org/geo/1.0/direct?",
    "cityNameParam": "q=",
    "cityNameValue": "",
    "stateParam": ",",
    "stateValue": "",
    "appidParam": "appid=",
    "appidValue": apiKey,


    /**  Check the REQUIRED parameters for this api call, urlBase, cityNameValue, appidValue are required */
    setUrl (urlBase, cityNameValue, stateValue, appidValue)
    {
        if (urlBase, cityNameValue, appidValue)
        {
            let cityState = this.cityNameParam + cityNameValue;

            if (stateValue)
            {
                cityState = this.cityNameParam + cityNameValue + this.stateParam + stateValue;
            }

            let url = urlBase + cityState + "&" + this.appidParam + appidValue;
            // url = encodeURIComponent(url);
            return url;
        }
    }
};


/**
* Take in the searched city and return the city's metadata, including the lattitude, longitude, country, state (where available)  
* @param {string} cityState - Searched for City's name and optional State
* @returns
*/
let setLatLongSearch = (cityState) =>
{
    // console.info("[ getLatAndLong ] : called");
    let search = { ...searchLatAndLong };
    let cityAndState = parseCityState(cityState);
    search.cityNameValue = cityAndState[0];
    search.stateValue = cityAndState[1];
    const requestURL = search.setUrl(search.urlBase, search.cityNameValue, search.stateValue, search.appidValue);

    fetchLatLong(requestURL);

}; //  [ end : getLatAndLong ]


/**
* Takes in a requestURL value and returns a JSON
* @param {string} requestURL - Endpoint of the API in the attempted call
* @returns {JSON} JSON Object of the data that are returned from the API endpoint
*/
let fetchLatLong = (requestURL) =>
{
    // console.info("[ fetchLatLong ] : called");

    if (requestURL)
    {
        fetch(requestURL)
            .then((response) =>
            {
                if (response.ok || response.status == 200)
                {
                    return response.json();
                }
                else
                {
                    throw new Error("The response was not okay: " + response.status);
                }
            }).then((data) =>
            {
                if (data.length > 0)
                {
                    let cityData = parseCityData(data);
                    setWeatherSearch(cityData);
                }
                else
                {
                    throw new Error("Check if the input city and/or state.");
                }
            })
            .catch((error) =>
            {
                alert("There was an error! \n\n" + error);
                console.error(error);
            });
    }
}; //  [ end : fetchLatLong ]


/**
* Set the city object to the data that are returned from the fetchLatLong search
* @param {JSON} data - Response data
* @returns City Object
*/
let parseCityData = (data) =>
{
    // console.info("[ extractPromiseData ] : called", data);

    // let city = Object.assign({}, cityMetadata);
    let currentCity = { ...cityMetadata };
    currentCity.city = data[0].name;
    currentCity.country = data[0].country;
    currentCity.state = data[0].state;
    currentCity.lattitude = data[0].lat;
    currentCity.longitude = data[0].lon;

    appendSearchHistory(currentCity);

    return currentCity;

}; //  [ end : extractPromiseData ]


//
// #endregion Lat and Long Search


// #region Parse City State
//


/**
 * Parses an input string of a City and State
 * @param {string} cityState - City and State formatted "cityName, stateName"; space after the comma is not required
 * @return {string} - Comma split values of City and State OR if there was no comma, then it returns the string passed in.
 */
let parseCityState = (cityState) =>
{
    // console.info("[ parseCityState ] : called");

    let cityAndState = [];

    const stateCodes = {
        AL: "Alabama",
        AK: "Alaska",
        AZ: "Arizona",
        AR: "Arkansas",
        CA: "California",
        CO: "Colorado",
        CT: "Connecticut",
        DE: "Delaware",
        FL: "Florida",
        GA: "Georgia",
        HI: "Hawaii",
        ID: "Idaho",
        IL: "Illinois",
        IN: "Indiana",
        IA: "Iowa",
        KS: "Kansas",
        KY: "Kentucky",
        LA: "Louisiana",
        ME: "Maine",
        MD: "Maryland",
        MA: "Massachusetts",
        MI: "Michigan",
        MN: "Minnesota",
        MS: "Mississippi",
        MO: "Missouri",
        MT: "Montana",
        NE: "Nebraska",
        NV: "Nevada",
        NH: "New Hampshire",
        NJ: "New Jersey",
        NM: "New Mexico",
        NY: "New York",
        NC: "North Carolina",
        ND: "North Dakota",
        OH: "Ohio",
        OK: "Oklahoma",
        OR: "Oregon",
        PA: "Pennsylvania",
        RI: "Rhode Island",
        SC: "South Carolina",
        SD: "South Dakota",
        TN: "Tennessee",
        TX: "Texas",
        UT: "Utah",
        VT: "Vermont",
        VA: "Virginia",
        WA: "Washington",
        WV: "West Virginia",
        WI: "Wisconsin",
        WY: "Wyoming"
    };

    if (cityState)
    {
        try
        {
            const re = /\s*,\s*/;
            cityAndState = cityState.split(re);
            try
            {
                let stateName = stateCodes[cityAndState[1]];
                cityAndState[1] = stateName;
            } catch (error)
            {
                // return the original value in cityAndState[1]
            }

        } catch (error)
        {
            console.warn("No State Passed in");
        }
    }
    else
    {
        cityAndState[0] = cityState;
    }

    return cityAndState;

}; //  [ end : parseCityState ]


//
// #endregion Parse City State


// #region Weather Search
//


/** Icon Object */
const iconBase = {
    code: "",
    description: "",
    urlBase: "https://openweathermap.org/img/w/",
    urlPost: ".png"
};


/**
 * Set the search parameters for a search to the api url
 * @property {string} urlBase - The API's url string that is used for making calls
 * @property {string} lattitudeParam - The API's parameter name used in the call for lattitude
 * @property {float} lattitudeValue - The searched cities lattitude
 * @property {string} longitudeParam - The API's parameter name used in the call for longitude
 * @property {float} longitudeValue - The searched cities longitude
 * @property {string} units - The temperature in Fahrenheit and wind speed in miles/hour
 * @property {string} appidParam - The API key that is used to make the call 
 * @property {string} appidValue - The API Key value to authenticate the call 
 * 
 */
const searchWeather = {
    "urlBase": 'https://api.openweathermap.org/data/2.5/forecast?'
    , "lattitudeParam": "lat="
    , "lattitudeValue": 0
    , "longitudeParam": "lon="
    , "longitudeValue": 0
    , "units": "units=imperial"
    , "limit": "limit=5"
    , "appidParam": "appid="
    , "appidValue": apiKey



    /**  Check the REQUIRED parameters for this api calllattitude, longitude, appidValue are required */
    , setURL (lattitude, longitude, appidValue)
    {
        if (this.lattitude, this.longitude, this.appidValue)
        {
            let searchUrl = this.urlBase + this.lattitudeParam + this.lattitudeValue + '&' + this.longitudeParam + this.longitudeValue + '&' + this.units + '&' + this.limit + '&' + this.appidParam + this.appidValue;

            //searchUrl = encodeURIComponent(searchUrl);
            return searchUrl;
        }
    } //  [ end : setUrlSearch ]
};


/**
* Set the Search Params for the Weather Search
* @param {object} cityData - an instance of {cityMetadata} 
*/
let setWeatherSearch = (cityData) =>
{
    // console.info("[ setWeatherSearch ] : called");
    let search = { ...searchWeather };
    search.lattitudeValue = cityData.lattitude;
    search.longitudeValue = cityData.longitude;

    const requestUrl = search.setURL(search.urlBase, search.longitudeValue, search.longitudeValue);

    fetchWeather(cityData, requestUrl);

}; //  [ end : setWeatherSearch ]


/**
* Get the Weather for the city searched for
* @param {object} cityData - an instance of {cityMetadata} 
* @param {string} requestURL
* @returns The current weather conditions for the city
*/
let fetchWeather = (cityData, requestURL) =>
{
    // console.info("[ fetchWeather ] : called");
    if (requestURL)
    {
        fetch(requestURL)
            .then((response) =>
            {
                return response.json();

            }).then((data) =>
            {
                renderCurrentWeather(cityData, data.list[0]);
                renderFiveDayForecast(data.list);
            })
            .catch((error) =>
            {
                alert("There was an error! \n\n See the console for details");
                console.error(error);
            });
    }

}; //  [ end : fetchWeather ]


//
// #endregion Weather Search


// #region Date
//


/**
 * A Date object built from DayJS
 * @property {dayjs} today - Current day object that DayJs creates
 * @property {string} todayFormat - Format representing how the dates render
 * @property {string} startDate - Begining range for rendering Five Day Forecast
 * @property {string} endDate - End range for rendering Five Day Forecast
 * 
 */
const currentDate = {
    "current": dayjs(),
    "dayOfWeek": "",
    "dayOfWeekFormat": "dddd",
    "currentDate": "",
    "currentDateFormat": "DD MMM, YYYY",
    "startDate": "",
    "endDate": "",

    /**
     * 
     * @returns Formatted Date Object
     */
    initDate ()
    {
        this.dayOfWeek = this.current.format(this.dayOfWeekFormat);
        this.currentDate = this.current.format(this.currentDateFormat);

        return this.date;
    },
    /**
     * Sets Start Date and End Date default values
     */
    getDateRange ()
    {
        this.startDate = this.current.add(1, 'day').startOf('day').unix();
        this.endDate = this.current.add(6, 'day').startOf('day').unix();
    },
    /** 
     * Takes in a string and format's it based on the dateFormat
     * @param {string} dateValue - Date value to be formatted
     * @returns {string} - fomatted date value
     */
    formatDate (dateValue)
    {
        return dayjs(dateValue).format(this.currentDateFormat);
    },
    /** 
     * Takes in a string and format's it based on the dateFormat
     * @param {string} dateValue - Date value to be formatted
     * @returns {string} - fomatted DAY value
     */
    formatDay (dateValue)
    {
        return dayjs(dateValue).format(this.dayOfWeekFormat);
    }
};


//
// #endregion Date  


// #region Render Components
//


/**
* Render the currentConditions information on the page
* @param {object} cityData - An instance of {cityMetadata} 
* @param {object} currentConditions - The data that are returned from the api call 
*/
let renderCurrentWeather = (cityData, currentConditions) =>
{
    // console.info("[ renderCurrentWeather ] : called");
    // Date
    // Set The Header Information
    elCurrentCondCard.classList.remove("hidden");
    elCurrentCondCity.textContent = cityData.city;
    elCurrentCondState.textContent = ", " + cityData.state;
    elCurrentDate.textContent = " ( " + dayjs().format("dddd - DD MMM, YYYY") + " )";
    // Set The Card Information
    elCurrentCondTemp.textContent = currentConditions.main.temp;
    elCurrentCondFeelsLike.textContent = currentConditions.main.feels_like;
    elCurrentCondWindSpeed.textContent = currentConditions.wind.speed;
    elCurrentCondHumidity.textContent = currentConditions.main.humidity;
    // Set The Icon For Current Conditions
    let icon = { ...iconBase };
    icon.icon = currentConditions.weather[0].icon;
    icon.description = currentConditions.weather[0].description;
    icon.url = icon.urlBase + icon.icon + icon.urlPost;

    elCurrentCondIcon.setAttribute("src", icon.url);
    elCurrentCondIcon.setAttribute("alt", icon.description);

}; //  [ end : renderCurrentWeather ]


/**
* Renders the Five-Day Forecast
* @param {object} forecast - The data that are returned from the api call  
*/
let renderFiveDayForecast = (forecast) =>
{
    // console.log("[ renderFiveDay ] : called");

    elFiveDayCard.classList.remove("hidden");

    let now = { ...currentDate };

    elFiveDayCard.innerHTML = "";

    let heading = document.createElement("h2");
    heading.textContent = "Five Day Forecast";
    heading.id = "five-day-title";
    elFiveDayCard.append(heading);

    for (let i = 0; i < forecast.length; i++)
    {
        now.getDateRange();

        if (forecast[i].dt >= now.startDate && forecast[i].dt < now.endDate)
        {
            if (forecast[i].dt_txt.slice(11, 13) == 12)
            {
                renderSingleForecast(forecast[i]);
            }
        }
    }


}; //  [ end : renderFiveDayForecast ]


/**
* Renders a Single Item From The Five Day Forecast
* @param {object} forecastItem
*/
let renderSingleForecast = (forecastItem) =>
{
    // console.log("[ renderSingleForecast ] : called");
    // Set The Icon For Current Conditions
    let icon = { ...iconBase };
    icon.icon = forecastItem.weather[0].icon;
    icon.description = forecastItem.weather[0].description;
    icon.url = icon.urlBase + icon.icon + icon.urlPost;
    // Set The Date
    let current = { ...currentDate };
    let day = current.formatDay(forecastItem.dt_txt);
    let date = current.formatDate(forecastItem.dt_txt);
    // Set The Item Values
    const temp = forecastItem.main.temp;
    const humidity = forecastItem.main.humidity;
    const windSpeed = forecastItem.wind.speed;

    let forecastItemContainer = document.createElement("div");
    let forecastItemTitle = document.createElement("h3");
    let forecastDisplayDate = document.createElement("h4");
    let forecastItemIcon = document.createElement("img");
    let forecastItemTemp = document.createElement("p");
    let forecastItemHumidity = document.createElement("p");
    let forecastItemWindSpeed = document.createElement("p");

    forecastItemContainer.append(forecastItemTitle,
        forecastDisplayDate,
        forecastItemIcon,
        forecastItemTemp,
        forecastItemHumidity,
        forecastItemWindSpeed);

    forecastItemTitle.textContent = day;
    forecastDisplayDate.textContent = date;
    forecastItemIcon.setAttribute("src", icon.url);
    forecastItemIcon.setAttribute("alt", icon.description);
    forecastItemTemp.textContent = "Temp: " + temp + " °F";
    forecastItemWindSpeed.textContent = "Wind Speed: " + windSpeed + " MPH";
    forecastItemHumidity.textContent = "Humidity: " + humidity + "%";

    forecastItemContainer.classList.add("flex-item");

    elFiveDayCard.append(forecastItemContainer);

}; //  [ end : renderSingleForecast ]


//
// #endregion Render Components


// #region History
//


/**
* Sets the current city to the {searchHistory} object. The City is only added if the City Name is unique.
* @param {object} cityData The searched for City's Name 
*/
let appendSearchHistory = (cityData) =>
{
    // console.info("[ appendSearchHistory ] : called");
    if (cityData)
    {
        if (!searchHistory.cities.some(currentCity => currentCity.city === cityData.city))
        {
            searchHistory.cities.push(cityData);
            writeToLocalStorage(searchHistory.cities);
        }
        // else
        // {
        //     searchHist.cities.unshift(cityData);
        // }

        renderHistory();
    }
}; //  [ end : appendSearchHistory ]


/**
* Render The Search History Buttons
*/
let renderHistory = () =>
{
    // console.info("[ renderHistory ] : called", searchHistory);
    searchHist.innerHTML = "";

    for (let i = searchHistory.cities.length - 1; i >= 0; i--)
    {
        let historyButton = document.createElement("button");
        historyButton.setAttribute("type", "button");
        historyButton.setAttribute("data-search", searchHistory.cities[i].city);
        historyButton.classList.add("hist-button");
        historyButton.textContent = (searchHistory.cities[i].city);
        searchHist.append(historyButton);
    }
}; //  [ end : renderHistory ]


/**
* Initializes the Search History in Local Storage
*/
let initHistory = () =>
{
    // console.log("[ initHistory ] : called");
    let storedHistory = localStorage.searchHistory;
    if (storedHistory)
    {
        searchHistory.cities = JSON.parse(storedHistory);
    }
    renderHistory();

}; //  [ end : initHistory ]

//
// #endregion History


// #region Local Storage
//


/**
* Read From Local Storage
* @param {string} searchValue - This is the String Value that will be parsed 
*/
let readFromLocalStorage = (searchValue) =>
{
    // console.log("[ readFromLocalStorage ] : called");
    let storedHist = localStorage.searchHistory;

    if (storedHist)
    {
        storedHist = JSON.parse(storedHist);
    }
    renderHistory();

}; //  [ end : readFromLocalStorage ]


/**
* Write To Local Storage
* @param {json} valueToWrite - This is the JSON object that will be stringified and saved to local storage
*/
let writeToLocalStorage = function (valueToWrite)
{
    // console.log("[ writeToLocalStorage ] : called");
    if (valueToWrite)
    {
        localStorage.searchHistory = JSON.stringify(valueToWrite);
    }
}; //  [ end : writeToLocalStorage ]


//
// #endregion Local Storage


/**
* Gets the input from the search bar; starts the search process to get the weather data
*/
let getSearchValue = () =>
{
    // console.info("[ getSearchValue ] : called");
    const searchValue = searchInput.value.trim();

    if (!searchValue)
    { // Only the City value is required, so I am only checking for that.
        alert("Please, enter in a value to search! \n\nAccepted Searches: by City OR by City, US State Code");
        return;
    }

    setLatLongSearch(searchValue);

}; //  [ end : getSearchValue ]


/**
* Gets the Historical Search Data to recall the selected search
* @param event - Browser Click Event
*/
let getHistoricalSearchValue = (event) =>
{
    // console.log("[ getHistoricalSearchValue ] : called");

    if (!event.target.matches(".hist-button"))
    {
        return;
    }

    let historicalButton = event.target;
    let historicalCity = historicalButton.getAttribute("data-search");
    let recalledSearch = searchHistory.cities.find(city => city.city === historicalCity);

    setWeatherSearch(recalledSearch);
}; //  [ end : getHistoricalSearchValue ]

// #region On Click and Init
//

initHistory();
searchButton.addEventListener("click", getSearchValue);
searchHist.addEventListener("click", getHistoricalSearchValue);

//
// #endregion On Click

